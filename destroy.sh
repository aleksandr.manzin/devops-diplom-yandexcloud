#!/usr/bin/env bash
echo -en "\033[0;32Destroying infractructure...\n"
echo -en "\033[0m"
cd terraform
terraform destroy -auto-approve

echo -en "\033[0;32Destroing s3 bucket...\n"
echo -en "\033[0m"
cd bucket
terraform destroy -auto-approve
