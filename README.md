# Дипломный практикум в YandexCloud

---
### Регистрация доменного имени

Шаги:
  - в качестве регистратора был выбран [nic.ru](https://nic.ru);

  - приобретено доменное имя **manzin.ru**;

  - в личном кабинете регистратора домен был делегирован на *DNS-сервера YandexCloud*:

    ![](screenshots/registrator.png "ns1.yandexcloud.net")

  - сначала был приобретен *DNS-хостинг* у регистратора, но в дальнейшем решил управлять *DNS-записями* через *YandexCloud*, т.к. с помощью *terraform* все делается в автоматическом режиме и позволяет избавить от муторной ручной работы

---

### Создание инфраструктуры

  Первоначально было решено, что для поднятия инфрастуктуры не будет использоваться *Oauth-токен*. Поэтому первым делом, c помощью *YandexCloud CLI*, был создан сервисный аккаунт `servicebot` с правами администратора. Дальнейшее разворачивание инфраструктуры производится с помощью сервисного аккаунта и *IAM-токена*.

  ![](screenshots/servicebot.png "Service Account")

  Вся инфраструктура разделена на две части:

  - в первой части описан сам бакет (вынесен в отдельную папку):

      - создаётся сервисный аккаунт для `s3 bucket` в *YandexCloud* и назначается роль `storage.editor`:

        ![](screenshots/storage.png "Storage bot")

      - создается статический ключ доступа для `s3 bucket`;

      - создается `s3 bucket` с помощью созданного ранее статического ключа;

  - во второй части описано поднятие всей инфрастуктуры:

    - созданы `dns-зона` и `A-записи` для каждого ресурса:
      ```tf
      resource "yandex_dns_zone" "dns-zone" {
        name        = "manzin-public-zone"
        description = "Public zone"

        labels = {
          label1 = "public-zone"
        }

        zone   = "manzin.ru."
        public = true

      }

      resource "yandex_dns_recordset" "rs" {
        count   = 6
        zone_id = yandex_dns_zone.dns-zone.id
        name    = local.dns[count.index]
        type    = "A"
        ttl     = 200
        data    = [yandex_vpc_address.vpc_address.external_ipv4_address[0].address]
      }
      ```

    - арендуется статический `IP-адрес`:
      ```tf
      resource "yandex_vpc_address" "vpc_address" {
        name = "ipv4_address-${terraform.workspace}"
        external_ipv4_address {
          zone_id = var.yc_region
        }
      }
      ```
    - настроена статическая маршрутизация:
      ```tf
      resource "yandex_vpc_route_table" "vpc_route_table" {
        name       = "vpc-route-table"
        network_id = yandex_vpc_network.vpc_network.id

        static_route {
          destination_prefix = "0.0.0.0/0"
          next_hop_address   = local.reverse_ip[terraform.workspace]
        }
      }
      ```

    - создаётся 3 подсети. Учитывается `workspace` (*default*, *stage* и *prod*), для каждого из которых используются свои диапазоны `IP-адресов`:
      ```tf
      resource "yandex_vpc_subnet" "vpc_subnet" {
        count          = 3
        name           = "subnet-${format(var.count_format, count.index + 1)}-${terraform.workspace}"
        network_id     = yandex_vpc_network.vpc_network.id
        zone           = local.zone[count.index]
        route_table_id = yandex_vpc_route_table.vpc_route_table.id
        v4_cidr_blocks = local.vpc_subnets[terraform.workspace][count.index]
      }
      ```

    - поднимается 7 виртуальных машин, одной из которых присваивается публичный `IP-адрес`:
      ```tf
      ...

      network_interface {
        subnet_id      = yandex_vpc_subnet.vpc_subnet[0].id
        ip_address     = local.reverse_ip[terraform.workspace]
        nat_ip_address = yandex_vpc_address.vpc_address.external_ipv4_address[0].address
        nat            = true
      }
      ```

    - на каждой из виртуальных машин создаётся пользователь *`aleksandr`* и пробрасывается публичный ключ:
      ```yml
      #cloud-config
      users:
        - name: aleksandr
          groups: sudo
          shell: /bin/bash
          sudo: ['ALL=(ALL) NOPASSWD:ALL']
          ssh-authorized-keys:
            - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCyzr8/8v6YEb3AN7r5Kh/I8cSIoM6DuaI6QHy9zhxUFvRkCGdKmMx+3JYXryFD/yZ5iVXNWQdqm8sJGFYuSTbNMtZbQ6UkKrrAvT1BlHrpIznqet2oWq9g3jz2L7I3ohmklWzahJMAhegzo4ppCaorxYu11cLH1026oxr5VcQGTHO5tw5Y9rXt6RGZiFO0un7KVXOrkIT3ZUO0CjX2mmqgorABIzh+YXtTMTf9yM65o/GzrcR7qNpCw6J4QpGj4FOpJfE57gN81yGNp5cZA4ywmboHizH8GJcGJgzr5MahxnRzNo7VcaQyRsQoHRGIY7yC882QEW/9ulrzgdMZgTM+hw4G5Ibj0cUFYm6c539QFIRyHPR8xwM8PEfyDtzBfX65T/BVpvS+w7C5l2OtAqjGSHu8jAcWsLWfsxK83NibHS5UpWUvFzip6eWTmp3a/Br/XTiQ/L4DJWp66X+G3ki30UOt/y+wOHQTX150B5iWFtGx3zmXUBs3bjwQT58gWJh55uiYr/o4rWPfvKN27M2ITa2IgrZp4g4UzTIyrTE13q4uC80Eg57c+iUT1jmYQ+aKYXLccTnyZgpYqb3w+AwU1toLt6vpoQtOCzuSNUNer1y38N1CSuC2YZjTcai1K40BCtYRB6ahl0ou804jwj3UZvcacEOxHqHKI6yBEew4bQ== aleksandr@manzin
      ```

    - описаны необходимые `variables` и `locals` переменные:
      ```yml
      variable "yc_cloud_id" {
        default = "b1gjp5l6o4ehob5b3eak"
      }

      variable "yc_folder_id" {
        default = "b1gqttie2maekdbe2nni"
      }

      variable "yc_region" {
        default = "ru-central1-a"
      }

      variable "count_format" {
        default = "%02d"
      }

      variable "hostname" {
        default = "manzin.ru"
      }

      locals {

        boot_disk_type = {
          default = "network-hdd"
          stage   = "network-hdd"
          prod    = "network-nvme"
        }

        boot_disk_size = {
          default = "10"
          stage   = "10"
          prod    = "20"
        }

        vpc_subnets = {
          default = {
            "0" = ["10.0.0.0/24"]
            "1" = ["10.0.1.0/24"]
            "2" = ["10.0.2.0/24"]
          }
          stage = {
            "0" = ["10.1.0.0/24"]
            "1" = ["10.0.1.0/24"]
            "2" = ["10.1.2.0/24"]
          }
          prod = {
            "0" = ["10.10.0.0/24"]
            "1" = ["10.10.1.0/24"]
            "2" = ["10.10.2.0/24"]
          }
        }

        reverse_ip = {
          default = "10.0.0.50"
          stage   = "10.1.0.50"
          prod    = "10.10.0.50"
        }

        zone = ["ru-central1-a", "ru-central1-b", "ru-central1-c"]

        dns = ["@", "www", "gitlab", "prometheus", "grafana", "alertmanager"]
      }
      ```

    - осуществляется вывод на экран внешнего и внутренних `IP-адресов` виртуальных машин, а также создается *`inventory`-файл*, необходимый для `ansible`:

      ![](screenshots/outputs.png "Outputs")


  - для удобства был написан `bash`-скрипт, который производит все необходимые действия для разворачивания инфрастуктуры:
    - поднимается сервисный аккаунт и `s3 bucket` и выгружается `access_key` и `static_key` в `backend.tfvars`;
    - производится инициализациия `terraform` с использованием ключей, записанных в `backend.tfvars`;
    - создаётся 2 `workspace'a` - *stage* и *prod*;
    - скрипт предлагает выбрать в каком из `workspace'ов` будет производиться деплой:

      ![](screenshots/interactive-mode.png "Interactive Mode")

    - осуществляется деплой;
    - запускается `ansible-playbook`

    ```bash
    #!//usr/bin/env bash

    # Initializing s3 bucket
    cd terraform/bucket/
    terraform init
    terraform apply -auto-approve

    # Adding access_key and secret_key to backend.tfvars
    access=($(grep access_key terraform.tfstate | cut -d, -f 1))
    secret=($(grep secret_key terraform.tfstate | cut -d, -f 1))
    echo "access_key = ${access[5]}" > ../backend.tfvars
    echo "secret_key = ${secret[5]}" >> ../backend.tfvars
    cd ..

    # Reinitializing
    terraform init -reconfigure -backend-config backend.tfvars

    # Creating workspaces
    terraform workspace new prod
    terraform workspace new stage

    # Choosing workspace
    PS3="You need to choose one of workspaces where to deploy: "
    echo "Terraform workspaces: "
    select workspace in "prod" "stage"
    do
      terraform workspace select $workspace
      echo
      echo -en "\033[0;32mSwitched to workspace "${workspace}"\n"
      break
    done

    # Deploying in choosed workspace
    terraform init
    terraform apply -auto-approve

    echo ""
    echo -en "\033[0;32mInstance List...\n"
    echo -en "\033[0;34m"
    yc compute instance list

    echo ""
    echo -en "\033[0;32mDNS zone list records...\n"
    echo -en "\033[0;34m"
    yc dns zone list-records manzin-public-zone

    echo -en "\033[0m"
    echo -en "\033[0;32mConfigure Virtual Machines...\n"
    echo -en "\033[0m"
    sleep 30s
    cd ../ansible
    ansible-playbook provision.yml
    ```

    <details>
    <summary>
    <font color=1164B4>скриншоты</font>
    </summary>

    ![](screenshots/Infrastructure.png "Infrastructure")

    ![](screenshots/virtualmachines.png "Virtual Machines")

    ![](screenshots/workspaces.png "Object Storage YandexCloud")

    ![](screenshots/subnets.png "VPC Subnets")

    ![](screenshots/static-ip.png "Static IP-address")

    ![](screenshots/cloud-dns.png "Cloud DNS")

    </details>

---

### Установка Nginx и LetsEncrypt

  - была развернута *ansible-роль* для установки `Nginx`, `Certbot` и настройки `Reverse proxy`. В качестве примера и основы использовалась роль:

    - [hispanico](https://github.com/hispanico/ansible-nginx-revproxy)

  - также изучались и другие роли на `github`, такие как:

    - [geerlingguy](https://github.com/geerlingguy/ansible-role-certbot)
    - [coopdevs](https://github.com/coopdevs/certbot_nginx)

  - в данной роли часть переменных создаётся *terraform\'ом*, а часть переменных описана в файле `REVERSE PROXY/defaults/main.yml` в виде словаря, который содержит сайты для реверс-прокси:

    ```yml
    ---
    # defaults file for Reverse_Proxy
    nginx_revproxy_sites:
      www.manzin.ru:
        domains:
          - www.manzin.ru
          - manzin.ru
        upstreams:
          - { backend_address: "{{ app_internal_ip }}", backend_port: 80 }
        ssl: true
        letsencrypt: true
        letsencrypt_email: "furunduk@gmail.com"
      gitlab.manzin.ru:
        client_max_body_size: "256M"
        proxy_read_timeout: "360"
        domains:
          - gitlab.manzin.ru
        upstreams:
          - { backend_address: "{{ gitlab_internal_ip }}", backend_port: 80 }
        ssl: true
        letsencrypt: true
        letsencrypt_email: "furunduk@gmail.com"
      grafana.manzin.ru:
        client_max_body_size: "256M"
        proxy_read_timeout: "360"
        domains:
          - grafana.manzin.ru
        upstreams:
          - { backend_address: "{{ monitoring_internal_ip }}", backend_port: 3000 }
        ssl: true
        letsencrypt: true
        letsencrypt_email: "furunduk@gmail.com"
      prometheus.manzin.ru:
        client_max_body_size: "256M"
        proxy_read_timeout: "360"
        domains:
          - prometheus.manzin.ru
        upstreams:
          - { backend_address: "{{ monitoring_internal_ip }}", backend_port: 9090 }
        ssl: true
        letsencrypt: true
        letsencrypt_email: "furunduk@gmail.com"
      alertmanager.manzin.ru:
        client_max_body_size: "256M"
        proxy_read_timeout: "360"
        domains:
          - alertmanager.manzin.ru
        upstreams:
          - { backend_address: "{{ monitoring_internal_ip }}", backend_port: 9093 }
        ssl: true
        letsencrypt: true
        letsencrypt_email: "furunduk@gmail.com"

    nginx_revproxy_de_activate_sites: false
    nginx_revproxy_remove_webroot_sites: true
    ```

  - стоит отметить, что в роли в таске *letsencrypt.yml*, при генерации сертификатов, используется параметр `--test-cert`, который необходимо убрать, если запускается на *prod-окружение*.

  - в результате выполнения данной роли, мы получаем доступные сайты со сгенерированными сертификатами (на данном этапе с ошибкой *502 Bad Gateway* и тестовыми сертификатами).

  <details>
  <summary>
  <font color=1164B4>скриншоты</font>
  </summary>

  ![](screenshots/manzin502.png "WordPress")

  ![](screenshots/www502.png "WordPress")

  ![](screenshots/gitlab502.png "Gitlab")

  ![](screenshots/grafana502.png "Grafana")

  ![](screenshots/prometheus502.png "Prometheus")

  ![](screenshots/alertmanager502.png "Alert Manager")
  </details>

---

### Установка кластера MySQL

  - была развернута *ansible-роль*, которая поднимает на серверах `db01` и `db02` *MySQL* и производит его настройку. В качестве роли была взята за пример и основу роль [geerlingguy](https://github.com/geerlingguy/ansible-role-mysql) и изменена под себя;

  - данная роль позволяет развернуть `MySQL-кластер` на разных семействах *Linux*, будь то *RedHat*, *Debian* или *ArchLinux*;

  - часть переменных задана руками в `mysql/defaults/main.yml`. Часть переменных создаются *terraform\'ом* и помещаются в *inventory-файл* в разделе *vars*;

  - в результате разворачивается кластер `MySQL` с *master-ролью* на *db01.manzin.ru* и *slave-ролью* на *db02.manzin.ru*;

  - создаётся база `wordpress`:
    ```yml
    mysql_databases:
       - name: wordpress
         collation: utf8_general_ci
         encoding: utf8
         replicate: 1
    ```
  - указывается сервер с ролью `master`:
    ```yml
    mysql_replication_master: 'db01.manzin.ru'
    ```

  - также создаётся 2 пользователя:
    ```yml
    mysql_users:
       - name: wordpress
         host: '%'
         password: wordpress
         priv: 'wordpress.*:ALL PRIVILEGES'

       - name: replica
         password: replica
         priv: '*.*:REPLICATION SLAVE,REPLICATION CLIENT'

    mysql_replication_user:
      name: replica
      password: replica
    ```
  - пользователь *wordpress*, как и полагается по заданию, имеет пароль *wordpress* и полными правами на базу *wordpress*

  - пользователь `replica`, из под которого происходит репликация, создаётся с двумя привилегиями:
    - *REPLICATION SLAVE* - позволяет подключиться к серверу и запросить обновленные данные на *master\'е*;
    - *REPLICATION CLIENT* - позволяет использовать статистику (*SHOW MASTER STATUS;*, *SHOW REPLICA STATUS;*, *SHOW BINARY LOGS;*)

  - проверка статуса `master-сервера`:
    ```bash
    mysql> SHOW MASTER STATUS;
    +------------------+----------+--------------+------------------+-------------------+
    | File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
    +------------------+----------+--------------+------------------+-------------------+
    | mysql-bin.000002 |      157 | wordpress    |                  |                   |
    +------------------+----------+--------------+------------------+-------------------+
    1 row in set (0.00 sec)

    mysql> SHOW MASTER STATUS\G
    *************************** 1. row ***************************
                 File: mysql-bin.000002
             Position: 157
         Binlog_Do_DB: wordpress
     Binlog_Ignore_DB:
    Executed_Gtid_Set:
    1 row in set (0.00 sec)

    ```

  - проверка работы репликации на `Slave-сервере`:
    ```bash
    mysql> SHOW REPLICA STATUS\G
    *************************** 1. row ***************************
                 Replica_IO_State: Waiting for source to send event
                      Source_Host: db01.manzin.ru
                      Source_User: replica
                      Source_Port: 3306
                    Connect_Retry: 60
                  Source_Log_File: mysql-bin.000002
              Read_Source_Log_Pos: 157
                   Relay_Log_File: relay-bin.000005
                    Relay_Log_Pos: 373
            Relay_Source_Log_File: mysql-bin.000002
               Replica_IO_Running: Yes
              Replica_SQL_Running: Yes
                  Replicate_Do_DB:
              Replicate_Ignore_DB:
               Replicate_Do_Table:
           Replicate_Ignore_Table:
          Replicate_Wild_Do_Table:
      Replicate_Wild_Ignore_Table:
                       Last_Errno: 0
                       Last_Error:
                     Skip_Counter: 0
              Exec_Source_Log_Pos: 157
                  Relay_Log_Space: 746
                  Until_Condition: None
                   Until_Log_File:
                    Until_Log_Pos: 0
               Source_SSL_Allowed: No
               Source_SSL_CA_File:
               Source_SSL_CA_Path:
                  Source_SSL_Cert:
                Source_SSL_Cipher:
                   Source_SSL_Key:
            Seconds_Behind_Source: 0
    Source_SSL_Verify_Server_Cert: No
                    Last_IO_Errno: 0
                    Last_IO_Error:
                   Last_SQL_Errno: 0
                   Last_SQL_Error:
      Replicate_Ignore_Server_Ids:
                 Source_Server_Id: 1
                      Source_UUID: 2883467d-4ae7-11ed-a0f6-d00d1776b3fb
                 Source_Info_File: mysql.slave_master_info
                        SQL_Delay: 0
              SQL_Remaining_Delay: NULL
        Replica_SQL_Running_State: Replica has read all relay log; waiting for more updates
               Source_Retry_Count: 86400
                      Source_Bind:
          Last_IO_Error_Timestamp:
         Last_SQL_Error_Timestamp:
                   Source_SSL_Crl:
               Source_SSL_Crlpath:
               Retrieved_Gtid_Set:
                Executed_Gtid_Set:
                    Auto_Position: 0
             Replicate_Rewrite_DB:
                     Channel_Name:
               Source_TLS_Version:
           Source_public_key_path:
            Get_Source_public_key: 0
                Network_Namespace:
    1 row in set (0.00 sec)
    ```
  - `slave-хосты`:
    ```bash
    mysql> SHOW REPLICAS;
    +-----------+------+------+-----------+--------------------------------------+
    | Server_Id | Host | Port | Source_Id | Replica_UUID                         |
    +-----------+------+------+-----------+--------------------------------------+
    |         2 |      | 3306 |         1 | 2890ea62-4ae7-11ed-a6d6-d00d1f021c2e |
    +-----------+------+------+-----------+--------------------------------------+
    1 row in set (0.00 sec)
    ```


---

### Установка Wordpress

  - была написана роль для разворачивания *WordPress*;
  - в качестве веб-сервера используется *Apache*;
  - в файле `WORDPRESS/defaults/main.yml` определены все необхдоимые переменные:
    ```yml
    ---
    # defaults file for WordPress
    domain: "manzin.ru"
    db_host: "db01.manzin.ru"
    db_name: "wordpress"
    db_user: "wordpress"
    db_password: "wordpress"
    wordpress_dir: "/var/www"
    wordpress_url: "https://wordpress.org/latest.tar.gz"
    php_modules: [ 'php7.4', 'php7.4-mysql', 'php7.4-curl', 'php7.4-gd', 'php7.4-mbstring', 'php7.4-xml', 'php7.4-xmlrpc', 'php7.4-soap', 'php7.4-intl', 'php7.4-zip', 'libapache2-mod-php']
    ```
  - после установки *Apache* и *WordPress* создаётся файл конфигурации, в котором определено подключение к *MySQL Cluster*:
    ```yml
    define( 'DB_NAME', '{{ db_name }}' );

    /** MySQL database username */
    define( 'DB_USER', '{{ db_user }}' );

    /** MySQL database password */
    define( 'DB_PASSWORD', '{{ db_password }}' );

    /** MySQL hostname */
    define( 'DB_HOST', '{{ db_host }}' );
    ```

  - в результате выполнения данной роли имеем работаюющий сайт по адресу `https://www.manzin.ru`.

    <details>
    <summary>
    <font color=1164B4>скриншоты</font>
    </summary>

    ![](screenshots/wordpress-lang.png)

    ![](screenshots/wordpress-installation.png)

    ![](screenshots/wordpress-success.png)

    ![](screenshots/wordpress-login.png)

    ![](screenshots/wordpress-admin.png)

    ![](screenshots/wordpress.png)
    </details>

### Установка Gitlab CE и Gitlab Runner

- Gitlab
    - в качестве примера и основы была использована роль [geerlingguy](https://github.com/geerlingguy/ansible-role-gitlab)

    - все необходимые переменные указываются в файле `GITLAB/defaults/main.yml`:
      ```yml
      gitlab_domain: "{{ domain_name }}"
      gitlab_external_url: "http://{{ gitlab_domain }}/"
      gitlab_git_data_dir: "/var/opt/gitlab/git-data"
      gitlab_edition: "gitlab-ce"
      gitlab_version: ''
      gitlab_backup_path: "/var/opt/gitlab/backups"
      gitlab_config_template: "gitlab.rb.j2"
      gitlab_initial_root_password: "NeToLoGy"
      gitlab_runners_registration_token: "s1AM65-5sq5kezExnsFW"
      ```
    - также тут сразу был указан ключ регистрации токена, который мы получили создав через веб-морду:

      ![](screenshots/runner-token.png "Token")

- Gitlab Runner

    - в качестве примера и основы была использована роль [riemers](https://github.com/riemers/ansible-gitlab-runner)

    - переменные указываются в файле `GITLAB RUNNER/defaults/main.yml`. Тут же мы указываем *token*:
      ```yml
      # GitLab coordinator URL
      gitlab_runner_coordinator_url: 'http://gitlab.manzin.ru'
      # GitLab registration token
      gitlab_runner_registration_token: 's1AM65-5sq5kezExnsFW'
      ```

- после того, как мы развернули *gitlab* и *runner*, нам нужно проверить, подключился ли наш *runner*:

  ![](screenshots/gitlab-runner.png "Gitlab Runner")

  ![](screenshots/runner-info.png "Gitlab Runner Info")

- теперь нам нужно добавить приватную часть ключа. Для этого идём в *Admin - Settings - CI/CD - Variables* и добавляем наш ключ:

  ![](screenshots/private-sshkey.png "Private Key")

- создадим новый проект:

  ![](screenshots/gitlab-newproject.png "New Project")

- произведем первоначальную настройку на виртуальной машине с *wordpress*:
  ```
  aleksandr@app:/var/www/manzin.ru/wordpress$ git init
  Initialized empty Git repository in /var/www/manzin.ru/wordpress/.git/

  aleksandr@app:/var/www/manzin.ru/wordpress$ git config --global --list
  user.name=Aleksandr
  user.email=furunduk@gmail.com

  aleksandr@app:/var/www/manzin.ru/wordpress$ git remote add gitlab http://gitlab.manzin.ru/gitlab-instance-be1bc991/WordPress.git
  aleksandr@app:/var/www/manzin.ru/wordpress$ git remote -v
  gitlab	http://gitlab.manzin.ru/gitlab-instance-be1bc991/WordPress.git (fetch)
  gitlab	http://gitlab.manzin.ru/gitlab-instance-be1bc991/WordPress.git (push)

  aleksandr@app:/var/www/manzin.ru/wordpress$ git switch -C main
  Switched to a new branch 'main'

  aleksandr@app:/var/www/manzin.ru/wordpress$ git status
  On branch main

  No commits yet

  Untracked files:
    (use "git add <file>..." to include in what will be committed)
  	wp-admin/
  	wp-content/
  	wp-includes/

  aleksandr@app:/var/www/manzin.ru/wordpress$ git add .
  aleksandr@app:/var/www/manzin.ru/wordpress$ git commit -m "Initial commit"
  aleksandr@app:/var/www/manzin.ru/wordpress$ git push gitlab main
  Username for 'http://gitlab.manzin.ru': root
  Password for 'http://root@gitlab.manzin.ru':
  Enumerating objects: 3070, done.
  Counting objects: 100% (3070/3070), done.
  Delta compression using up to 4 threads
  Compressing objects: 100% (3005/3005), done.
  Writing objects: 100% (3070/3070), 19.15 MiB | 6.39 MiB/s, done.
  Total 3070 (delta 507), reused 0 (delta 0)
  remote: Resolving deltas: 100% (507/507), done.
  To http://gitlab.manzin.ru/gitlab-instance-be1bc991/WordPress.git
   * [new branch]      main -> main
  ```

  ![](screenshots/gitlab-repository.png)

- теперь можно создать файл конфигурации `.gitlab-ci.yml`. Для этого переходим в наш проект *WordPress - CI/CD - Editor* и пишем код:
  ```yml
  before_script:
    # Setup SSH deploy keys
    - 'which ssh-agent || ( apt update -y && apt install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$ssh_key")
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh

  stages:
    - deploy

  deploy-job:
    stage: deploy
    environment:
      name: WordPress
      url: https://manzin.ru
    script:
      - echo "Deploying application..."
      - ssh -o StrictHostKeyChecking=no aleksandr@app.manzin.ru sudo chown -R aleksandr:aleksandr /var/www/manzin.ru/wordpress
      - ssh -o StrictHostKeyChecking=no aleksandr@app.manzin.ru git config --global user.name "Aleksandr Manzin"
      - ssh -o StrictHostKeyChecking=no aleksandr@app.manzin.ru git config --global user.email "furunduk@gmail.com"
      - rsync -arvzP -e "ssh -o StrictHostKeyChecking=no" ./* aleksandr@app.manzin.ru:/var/www/manzin.ru/wordpress
      - ssh -o StrictHostKeyChecking=no aleksandr@app.manzin.ru sudo chown -R www-data:www-data /var/www/manzin.ru/wordpress
  ```

- Жмем *Commit changes* и видим, что наш *pipeline* создался и отработал:

  ![](screenshots/pipeline-created.png "Pipeline")

- возвращаемся обратно на виртуальную машину с *wordpress*, создаем файл и пушим в наш репозиторий:
  ```
  aleksandr@app:/var/www/manzin.ru/wordpress$ touch wp-content/test.file
  aleksandr@app:/var/www/manzin.ru/wordpress$ git status
  On branch main
  Untracked files:
    (use "git add <file>..." to include in what will be committed)
  	wp-content/test.file

  nothing added to commit but untracked files present (use "git add" to track)
  aleksandr@app:/var/www/manzin.ru/wordpress$ git add .
  aleksandr@app:/var/www/manzin.ru/wordpress$ git commit -m "Testing pipeline"
  [main ed038cf] Testing pipeline
  1 file changed, 0 insertions(+), 0 deletions(-)
  create mode 100644 wp-content/test.file
  aleksandr@app:/var/www/manzin.ru/wordpress$ git pull gitlab main
  Username for 'http://gitlab.manzin.ru': root
  Password for 'http://root@gitlab.manzin.ru':
  remote: Enumerating objects: 4, done.
  remote: Counting objects: 100% (4/4), done.
  remote: Compressing objects: 100% (3/3), done.
  remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
  Unpacking objects: 100% (3/3), 741 bytes | 741.00 KiB/s, done.
  From http://gitlab.manzin.ru/gitlab-instance-be1bc991/WordPress
  * branch            main       -> FETCH_HEAD
    085cb2c..7c6721e  main       -> gitlab/main
  Merge made by the 'recursive' strategy.
  .gitlab-ci.yml | 23 +++++++++++++++++++++++
  1 file changed, 23 insertions(+)
  create mode 100644 .gitlab-ci.yml
  aleksandr@app:/var/www/manzin.ru/wordpress$ git push gitlab main
  Username for 'http://gitlab.manzin.ru': root
  Password for 'http://root@gitlab.manzin.ru':
  Enumerating objects: 10, done.
  Counting objects: 100% (9/9), done.
  Delta compression using up to 4 threads
  Compressing objects: 100% (5/5), done.
  Writing objects: 100% (6/6), 716 bytes | 716.00 KiB/s, done.
  Total 6 (delta 1), reused 0 (delta 0)
  To http://gitlab.manzin.ru/gitlab-instance-be1bc991/WordPress.git
     7c6721e..dd1deab  main -> main
  ```

- Видим, что *Pipeline* успешно отработал:

  ![](screenshots/pipeline-job.png "Pipeline Job")

  ![](screenshots/pipeline-testing.png "Pipeline Testing")


### Установка Prometheus, Alert Manager, Node Exporter и Grafana

- `Prometheus`
  - роль устанавливает *Prometheus*;
  - создаёт службу *prometheus.service*;
  - подгружает файл настроек, в котором описываются *jobs* и сервера для сбора метрик:
    ```yml
    ---
    # my global config
    global:
      scrape_interval: 15s
      evaluation_interval: 15s

    alerting:
      alertmanagers:
        - static_configs:
            - targets: ["localhost:9093"]

    rule_files:
      - alerts.yml

    scrape_configs:
      - job_name: "prometheus"
        static_configs:
          - targets: ["localhost:9090"]

      - job_name: "alertmanager"
        static_configs:
          - targets: ["localhost:9093"]

      - job_name: "nodes"
        scrape_interval: 5s
        static_configs:
          - targets: ["manzin.ru:9100"]
          - targets: ["app.manzin.ru:9100"]
          - targets: ["db01.manzin.ru:9100"]
          - targets: ["db02.manzin.ru:9100"]
          - targets: ["gitlab.manzin.ru:9100"]
          - targets: ["runner.manzin.ru:9100"]
          - targets: ["monitoring.manzin.ru:9100"]

      - job_name: "MySQL"
        scrape_interval: 5s
        static_configs:
          - targets: ["db01.manzin.ru:9104"]
          - targets: ["db02.manzin.ru:9104"]
    ```
- `Alertmanager`
  - роль устанавливает *Alertmanager*;
  - создаёт службу *alertmanager.service*;
  - подгружает файл `alerts.yml`:
    ```yml
    ---
    groups:
      - name: alerts
        rules:
          # Alert for any instance that is unreachable for >5 minutes.
          - alert: InstanceDown
            expr: 'up == 0'
            for: 5m
            labels:
              severity: critical
            annotations:
              description: '{% raw %}{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 5 minutes.{% endraw %}'
              summary: '{% raw %}Instance {{ $labels.instance }} down{% endraw %}'

          # Alert for any instance that need toorestarted.
          - alert: RebootRequired
            expr: 'node_reboot_required > 0'
            labels:
              severity: warning
            annotations:
              description: '{% raw %}{{ $labels.instance }} requires a reboot.{% endraw %}'
              summary: '{% raw %}Instance {{ $labels.instance }} - reboot required{% endraw %}'

          # Alert for any instance that has a median request latency >1s.
          - alert: APIHighRequestLatency
            expr: api_http_request_latencies_second{quantile="0.5"} > 1
            for: 10m
            labels:
              severity: warning
            annotations:
              summary: "High request latency on {{ $labels.instance }}"
              description: "{{ $labels.instance }} has a median request latency above 1s (current value: {{ $value }}s)"

          - alert: HighLoad_5m
            expr: node_load1 > 8
            for: 5m
            labels:
              severity: critical
            annotations:
              summary: "Instance {{ $labels.instance }} under high load"
              description: "{{ $labels.instance }} of job {{ $labels.job }} is under high load last 5 minutes."

          - alert: HighLoad_1m
            expr: node_load1 > 8
            for: 1m
            labels:
              severity: warning
            annotations:
              summary: "Instance {{ $labels.instance }} under high load"
              description: "{{ $labels.instance }} of job {{ $labels.job }} is under high load last 1 minutes."
    ```
- `Node Exporter`
  - роль устанавливает *Node Exporter*;
  - создаёт службу *node_exporter.service*;
  - устанавливается на все сервера.

- `Grafana`:
  - роль устанавливает *Grafana*;
  - подгружает *datasource-config*;
  - подгружает *dashboard-config*

- дополнительно была создана роль `MySQLD Exporter` (в качестве примера и основы была взята роль [idealista](https://github.com/idealista/prometheus_mysqld_exporter_role/))
  - роль устанавливает *MySQLD Exporter*;
  - все переменные задаются в файле `MYSQLD EXPORTER/defaults/main.yml`, в том числе и для подключения к базе данных:
    ```yml
    mysqld_exporter_mysql_user: wordpress
    mysqld_exporter_mysql_password: wordpress
    mysqld_exporter_mysql_port: 3306
    mysqld_exporter_mysql_host: localhost
    ```
  - устанавливается исключительнон на сервере *MySQL Cluster*.

  ![](screenshots/prometheus-alerts.png "Prometheus Alerts")

  ![](screenshots/prometheus-targets.png "Prometheus Targets")

  ![](screenshots/alertmanager-alerts.png "Alertmanager Alerts")

  ![](screenshots/grafana-dashboards.png "Dashboards")

  ![](screenshots/node-exporter.png "Node Exporter")

  ![](screenshots/mysqld-exporter.png "MySQLD Exporter")

- отключим один из серверов:

  ![](screenshots/server-off.png)
