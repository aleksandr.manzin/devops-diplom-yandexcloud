resource "yandex_compute_instance" "app" {
  name                      = "app"
  zone                      = local.zone[0]
  hostname                  = "app.${var.hostname}"
  folder_id                 = var.yc_folder_id
  allow_stopping_for_update = true

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    initialize_params {
      name     = "app-${terraform.workspace}"
      image_id = data.yandex_compute_image.ubuntu_image.id
      type     = local.boot_disk_type[terraform.workspace]
      size     = local.boot_disk_size[terraform.workspace]
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vpc_subnet[0].id
  }

  metadata = {
    user-data = "${file("./meta.yml")}"
  }
}
