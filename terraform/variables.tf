variable "yc_cloud_id" {
  default = "b1gjp5l6o4ehob5b3eak"
}

variable "yc_folder_id" {
  default = "b1gqttie2maekdbe2nni"
}

variable "yc_region" {
  default = "ru-central1-a"
}

variable "count_format" {
  default = "%02d"
}

variable "hostname" {
  default = "manzin.ru"
}

locals {

  boot_disk_type = {
    default = "network-hdd"
    stage   = "network-hdd"
    prod    = "network-nvme"
  }

  boot_disk_size = {
    default = "10"
    stage   = "10"
    prod    = "20"
  }

  vpc_subnets = {
    default = {
      "0" = ["10.0.0.0/24"]
      "1" = ["10.0.1.0/24"]
      "2" = ["10.0.2.0/24"]
    }
    stage = {
      "0" = ["10.1.0.0/24"]
      "1" = ["10.0.1.0/24"]
      "2" = ["10.1.2.0/24"]
    }
    prod = {
      "0" = ["10.10.0.0/24"]
      "1" = ["10.10.1.0/24"]
      "2" = ["10.10.2.0/24"]
    }
  }

  reverse_ip = {
    default = "10.0.0.50"
    stage   = "10.1.0.50"
    prod    = "10.10.0.50"
  }

  zone = ["ru-central1-a", "ru-central1-b", "ru-central1-c"]

  dns = ["@", "www", "gitlab", "prometheus", "grafana", "alertmanager"]
}
