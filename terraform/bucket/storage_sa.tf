// создание сервисного аккаунта
resource "yandex_iam_service_account" "storage" {
  name = "storage"
}

// Назначение роли сервисному аккаунту
resource "yandex_resourcemanager_folder_iam_member" "storage-editor" {
  folder_id = var.yc_folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.storage.id}"
}

// Создание статического ключа доступа
resource "yandex_iam_service_account_static_access_key" "storage-static-key" {
  service_account_id = yandex_iam_service_account.storage.id
  description        = "static access key for object storage"
}
