variable "yc_cloud_id" {
  default = "b1gjp5l6o4ehob5b3eak"
}

variable "yc_folder_id" {
  default = "b1gqttie2maekdbe2nni"
}

variable "yc_region" {
  type    = string
  default = "ru-central1-a"
}

variable "yc_bucket" {
  default = "manzin-s3-bucket"
}
