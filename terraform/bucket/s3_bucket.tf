// создаем бакет и использованием статического ключа
resource "yandex_storage_bucket" "s3-bucket" {
  access_key = yandex_iam_service_account_static_access_key.storage-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.storage-static-key.secret_key
  bucket     = var.yc_bucket
}
