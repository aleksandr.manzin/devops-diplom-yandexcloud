resource "yandex_compute_instance" "db02" {
  name                      = "db02"
  zone                      = local.zone[1]
  hostname                  = "db02.${var.hostname}"
  folder_id                 = var.yc_folder_id
  allow_stopping_for_update = true

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    initialize_params {
      name     = "db02-${terraform.workspace}"
      image_id = data.yandex_compute_image.ubuntu_image.id
      type     = local.boot_disk_type[terraform.workspace]
      size     = local.boot_disk_size[terraform.workspace]
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vpc_subnet[1].id
  }

  metadata = {
    user-data = file("./meta.yml")
  }
}
