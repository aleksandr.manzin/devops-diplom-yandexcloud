resource "yandex_dns_zone" "dns-zone" {
  name        = "manzin-public-zone"
  description = "Public zone"

  labels = {
    label1 = "public-zone"
  }

  zone   = "manzin.ru."
  public = true

}

resource "yandex_dns_recordset" "rs" {
  count   = 6
  zone_id = yandex_dns_zone.dns-zone.id
  name    = local.dns[count.index]
  type    = "A"
  ttl     = 200
  data    = [yandex_vpc_address.vpc_address.external_ipv4_address[0].address]
}
