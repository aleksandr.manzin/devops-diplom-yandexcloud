output "yandex_vpc_address" {
  value = yandex_vpc_address.vpc_address.external_ipv4_address[0].address
}

output "db01_internal_ip" {
  value = yandex_compute_instance.db01.network_interface.0.ip_address
}

output "db02_internal_ip" {
  value = yandex_compute_instance.db02.network_interface.0.ip_address
}

output "reverse_proxy_internal_ip" {
  value = yandex_compute_instance.reverse_proxy.network_interface.0.ip_address
}

output "gitlab_internal_ip" {
  value = yandex_compute_instance.gitlab.network_interface.0.ip_address
}

output "runner_internal_ip" {
  value = yandex_compute_instance.runner.network_interface.0.ip_address
}

output "app_internal_ip" {
  value = yandex_compute_instance.app.network_interface.0.ip_address
}

output "monitoring_internal_ip" {
  value = yandex_compute_instance.monitoring.network_interface.0.ip_address
}
