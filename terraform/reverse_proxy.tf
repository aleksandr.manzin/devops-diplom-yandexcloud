resource "yandex_compute_instance" "reverse_proxy" {
  name                      = "reverse-proxy"
  zone                      = local.zone[0]
  hostname                  = var.hostname
  folder_id                 = var.yc_folder_id
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      name     = "proxy-${terraform.workspace}"
      image_id = data.yandex_compute_image.nat_instance_image.id
      type     = local.boot_disk_type[terraform.workspace]
      size     = local.boot_disk_size[terraform.workspace]
    }
  }

  network_interface {
    subnet_id      = yandex_vpc_subnet.vpc_subnet[0].id
    ip_address     = local.reverse_ip[terraform.workspace]
    nat_ip_address = yandex_vpc_address.vpc_address.external_ipv4_address[0].address
    nat            = true
  }

  metadata = {
    user-data = "${file("./meta.yml")}"
  }

  lifecycle {
    create_before_destroy = true
  }
}
