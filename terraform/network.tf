resource "yandex_vpc_address" "vpc_address" {
  name = "ipv4_address-${terraform.workspace}"
  external_ipv4_address {
    zone_id = var.yc_region
  }
}

resource "yandex_vpc_network" "vpc_network" {
  name = "net-${terraform.workspace}"
}

resource "yandex_vpc_route_table" "vpc_route_table" {
  name       = "vpc-route-table"
  network_id = yandex_vpc_network.vpc_network.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    next_hop_address   = local.reverse_ip[terraform.workspace]
  }
}

resource "yandex_vpc_subnet" "vpc_subnet" {
  count          = 3
  name           = "subnet-${format(var.count_format, count.index + 1)}-${terraform.workspace}"
  network_id     = yandex_vpc_network.vpc_network.id
  zone           = local.zone[count.index]
  route_table_id = yandex_vpc_route_table.vpc_route_table.id
  v4_cidr_blocks = local.vpc_subnets[terraform.workspace][count.index]
}
