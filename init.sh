#!//usr/bin/env bash

# Initializing s3 bucket
cd terraform/bucket/
terraform init
terraform apply -auto-approve

# Adding access_key and secret_key to backend.tfvars
access=($(grep access_key terraform.tfstate | cut -d, -f 1))
secret=($(grep secret_key terraform.tfstate | cut -d, -f 1))
echo "access_key = ${access[5]}" > ../backend.tfvars
echo "secret_key = ${secret[5]}" >> ../backend.tfvars
cd ..

# Reinitializing
terraform init -reconfigure -backend-config backend.tfvars

# Creating workspaces
terraform workspace new prod
terraform workspace new stage

# Choosing workspace
PS3="You need to choose one of workspaces where to deploy: "
echo "Terraform workspaces: "
select workspace in "prod" "stage"
do
  terraform workspace select $workspace
  echo
  echo -en "\033[0;32mSwitched to workspace "${workspace}"\n"
  break
done

# Deploying in choosed workspace
terraform init
terraform apply -auto-approve

echo ""
echo -en "\033[0;32mInstance List...\n"
echo -en "\033[0;34m"
yc compute instance list

echo ""
echo -en "\033[0;32mDNS zone list records...\n"
echo -en "\033[0;34m"
yc dns zone list-records manzin-public-zone

echo -en "\033[0m"
echo -en "\033[0;32mConfigure Virtual Machines...\n"
echo -en "\033[0m"
sleep 30s
cd ../ansible
ansible-playbook provision.yml
